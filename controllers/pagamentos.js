var logger = require('../servicos/logger.js');
module.exports = function (app) {
	const PAGAMENTO_CRIADO = 'CRIADO';
	const PAGAMENTO_CONFIRMADO = 'CONFIRMADO';
	const PAGAMENTO_CANCELADO = 'CANCELADO';

	app.get('/pagamentos',function (req, res) {
		console.log('Recebida a requisao de teste na porta 3000');
		res.send('ok');
	});


	app.get('/pagamentos/pagamento/:id',function (req, res) {
		var id = req.params.id;
		//console.log('Consultando pagamento ' + id);
		logger.info('Consultando pagamento ' + id);

		var memcachedClient = app.servicos.memcachedClient();

		memcachedClient.get('pagamento-' + id, function(erro, retorno){
			if(erro || !retorno){
				console.log('MISS - chave não encontrada');
				var connection = app.persistencia.connectionFactory();
				var pagamentoDao = new app.persistencia.PagamentoDao(connection);

				pagamentoDao.buscaPorId(id, function(erro,resultado){
					if(erro){
						console.log('Erro ao consultar no banco: ' + erro);
						res.status(500).send(erro);
						return;	
					}
					console.log('Pagamento encontrado ' + JSON.stringify(resultado));
					res.json(resultado); 
				});
			} else {
				console.log('Hit - valor: ' + JSON.stringify(retorno));
				res.json(retorno);
				return; 
			}
		});

	});


	app.delete('/pagamentos/pagamento/:id', function (req,res) {
		var pagamento = {};
		var id = req.params.id;
		
		pagamento.id = id;
		pagamento.status = PAGAMENTO_CANCELADO;

		var connection = app.persistencia.connectionFactory();
		var pagamentoDao = new app.persistencia.PagamentoDao(connection);

		pagamentoDao.atualiza(pagamento, function(erro){
			if(erro){
				res.status(500).send(erro);
				return;
			}
			console.log('Pagamento Cancelado');
			res.status(204).send(pagamento);
		});		
		
	});

	app.put('/pagamentos/pagamento/:id', function (req,res) {
		var pagamento = {};
		var id = req.params.id;
		
		pagamento.id = id;
		pagamento.status = PAGAMENTO_CONFIRMADO;

		var connection = app.persistencia.connectionFactory();
		var pagamentoDao = new app.persistencia.PagamentoDao(connection);

		pagamentoDao.atualiza(pagamento, function(erro){
			if(erro){
				res.status(500).send(erro);
				return;
			}
			console.log('Pagamento Confirmado');

			var response = {
				dados_da_confirmacao: pagamento,
				links: [
				{
					href: "http://localhost:3000/pagamentos/pagamento/" + pagamento.id,
					rel: "cancelar",
					method: "DELETE"
				}	
				]
			}


			res.send(response);
		});		
		
	});

	app.post("/pagamentos/pagamento",function(req, res) {
		
		req.assert("pagamento.forma_de_pagamento",
			"Forma de pagamento é obrigatorio").notEmpty();
		req.assert("pagamento.valor",
			"Valor é obrigatorio e deve ser um decimal").notEmpty().isFloat();
		req.assert("pagamento.moeda", 
			"Moeda é obrigatória e deve ter 3 caracteres").notEmpty().len(3,3);

		var errors = req.validationErrors();
		if(errors){
			console.log('Erros de validacao', errors);
			res.status(500).send(errors);
			return;
		}

		var pagamento = req.body["pagamento"];
		console.log('processando pagamento...');

		var connection = app.persistencia.connectionFactory();
		var pagamentoDao = new app.persistencia.PagamentoDao(connection);

		pagamento.status = PAGAMENTO_CRIADO;
		pagamento.data = new Date;

		
		pagamentoDao.salva(pagamento, function(erro, resultado){			
			
			if(erro){
				console.log('erro ao inserir no banco' + erro);
				res.status(400).send(erro);
			} else {
				
				pagamento.id = resultado.insertId;
				

				console.log('pagamento criado: ' + resultado);

				var memcachedClient = app.servicos.memcachedClient();
				memcachedClient.set('pagamento-' + pagamento.id, pagamento, 60000, function(erro) {
					console.log('Nova chave adicionada ao chace: pagamento-' + pagamento.id);
				});


				if(pagamento.forma_de_pagamento == 'cartao'){

					var cartao = req.body["cartao"];
					
					console.log(cartao);
					
					var clienteCartoes = new app.servicos.clienteCartoes();
					clienteCartoes.autoriza(cartao, function (exception, requeste, response, retorno) {
						
						if(exception){
							console.log(exception);
							res.status(400).send(exception);
							return;
						}

						console.log(retorno);

						res.location('pagamentos/pagamento/' + pagamento.id);

						var response = {
							dados_do_pagamento: pagamento,
							cartao: retorno,
							links: [
							{
								href: "http://localhost:3000/pagamentos/pagamento/" + pagamento.id,
								rel: "confirmar",
								method: "PUT"
							},
							{
								href: "http://localhost:3000/pagamentos/pagamento/" + pagamento.id,
								rel: "cancelar",
								method: "DELETE"
							}
							]
						}

						res.status(201).json(response);
						
						return;
					});
					

				} else {

					res.location('pagamentos/pagamento/' + pagamento.id);

					var response = {
						dados_do_pagamento: pagamento,
						links: [
						{
							href: "http://localhost:3000/pagamentos/pagamento/" + pagamento.id,
							rel: "confirmar",
							method: "PUT"
						},
						{
							href: "http://localhost:3000/pagamentos/pagamento/" + pagamento.id,
							rel: "cancelar",
							method: "DELETE"
						}
						]
					}

					res.status(201).json(response);

				}
			}
		});
	});
}




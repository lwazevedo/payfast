var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var morgan = require('morgan');
var logger = require('../servicos/logger.js');

module.exports = function () {
	var app = express();


	//Log
	app.use(morgan("common", {
		stream: {
			write: function(mensagem) {
				logger.info(mensagem);
			}
		}
	})); 

	//Faz parser para browser para urlEncoded 
	app.use(bodyParser.urlencoded({extended: true}));
	//Faz parser de json para requisiçoes
	app.use(bodyParser.json());

	//Verificar erros 
	app.use(expressValidator());

	//carrega os arquivos necessários
	consign()
		.include('controllers')
		.then('persistencia')
		.then('servicos')
		.into(app);

	return app;
	
}
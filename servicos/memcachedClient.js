var memcached = require('memcached');

module.exports = function() {

	return createMemcachedClient;
}

function createMemcachedClient() {
	// retries = tentativas por request
	// retry   = Tempo de espera em uma falha de servidor e subir novamente
	// remove  = Remove do poll cluster um no que esta morto
	var cliente = new memcached('localhost:11211', {
		retries: 10, 
		retry: 10000, 
		remove: true, 
	});
	return cliente;
}





